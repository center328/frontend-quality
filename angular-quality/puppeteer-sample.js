#!/bin/node

const puppeteer = require('puppeteer');

function delay(time) {
   return new Promise(function(resolve) {
       setTimeout(resolve, time)
   });
}

console.log('Creating shot dir');
var fs = require('fs');
var dir = './shots';

if (!fs.existsSync(dir)){
    fs.mkdirSync(dir);
}


(async () => {
  const browser = await puppeteer.launch({executablePath: '/usr/bin/chromium-browser'});
  const page = await browser.newPage();
  page.setViewport({width: 1280, height: 768});
  await page.goto('http://angular-quality.herokuapp.com/',  {waitUntil: 'networkidle2'});
  await delay(1000);
  await page.screenshot({path: 'shots/startpage.png'});
  //only available when running in headless mode!
  //await page.pdf({path: 'shots/startpage.pdf', format: 'A4'});

  await page.type('input[id=customerName]', 'customer');
  await page.type('input[id=customerId]', '123');
  await delay(100);
  await page.click('button.btn');
  await delay(500);

  await page.screenshot({path: 'shots/customer.png'});
  //await page.pdf({path: 'shots/customer.pdf', format: 'A4'});

  await browser.close();
})();
