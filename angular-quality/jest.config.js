module.exports = {
  "rootDir": ".",
  "setupTestFrameworkScriptFile": "<rootDir>/src/setup-jest.ts",
  "globals": {
    "ts-jest": {
      "tsConfigFile": "./src/tsconfig.spec.json"
    },
    "__TRANSFORM_HTML__": true
  },
  "transform": {
    "^.+\\.(ts|js|html)$": "<rootDir>/node_modules/jest-preset-angular/preprocessor.js"
  },
  "testMatch": [
    "<rootDir>/src/**/*.spec.ts",
    "<rootDir>/src/**/*.spec-jest.ts"
  ],
  "moduleFileExtensions": [
    "ts",
    "js",
    "html",
    "json"
  ],
  "coveragePathIgnorePatterns": [
    "<rootDir>/node_modules/",
    "<rootDir>/src/setup-jest.ts"
  ],
  "moduleNameMapper": {
    "\\.(jpg|jpeg|png)$": "<rootDir>/__mocks__/image.js",
    "@lib/(.*)": "<rootDir>/src/lib/$1"
  },
  "transformIgnorePatterns": [
    "node_modules/(?!@ngrx)"
  ],
  "modulePathIgnorePatterns": [
    "dist"
  ]
};
