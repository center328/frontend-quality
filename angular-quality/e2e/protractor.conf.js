// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const {SpecReporter} = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './src/**/*.e2e-spec.ts'
  ],

  multiCapabilities: [
    {
      browserName: 'Safari',
      platformName: 'iOS',
      platformVersion: '12.2',
      deviceName: 'iPhone X',
      automationName: 'XCUITest'
    },
    {
      avd: 'Nexus_5X_API_25',
      browserName: 'Chrome',
      platformName: 'Android',
      platformVersion: '7.1.1',
      deviceName: 'Android Emulator',
      automationName: 'Appium',
      skipDeviceInitialization: true,
      'goog:chromeOptions': {
        args: ["--no-first-run", "--disable-fre"]
      }
    }
  ],
  seleniumAddress: 'http://localhost:4723/wd/hub',
  baseUrl: 'http://10.76.17.63:4200/', // check if ip addr is corect
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () {
    }
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.e2e.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({spec: {displayStacktrace: true}}));
  }
};
