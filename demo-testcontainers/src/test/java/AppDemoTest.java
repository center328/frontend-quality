import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AppDemoTest {

    private AppDemo appDemo;

    @Before
    public void setUp() throws Exception {
        appDemo = new AppDemo();
    }

    @After
    public void tearDown() throws Exception {
        appDemo.tearDownLogic();
    }

    @Test
    public void add() {
        assertThat(appDemo.add(3,5),is(8));
    }
}